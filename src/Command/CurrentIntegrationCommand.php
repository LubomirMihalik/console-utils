<?php

namespace LubomirMihalik\ConsoleUtils\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CurrentIntegrationCommand extends Command
{
    protected function configure()
    {
        $this->setName('git:checkout_last_integration')
            ->setDescription('Search and switch to current integration branch');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $symfonyStyle = new SymfonyStyle($input, $output);

        $this->runCommand('fetch');
        $results = $this->runCommand('branch --no-color -a | grep integration');
        if (count($results) === 0) {
            $symfonyStyle->error('No integration branches found.');

            return;
        }

        $branches = [];
        foreach ($results as $result) {
            $tmp = explode('/', $result);
            $branches[] = array_pop($tmp);
        }

        sort($branches);
        $symfonyStyle->write(implode(', ', $branches));

        $lastIntegrationBranch = 'integration/' . array_pop($branches);
        $symfonyStyle->success(
            sprintf('Searched %s integrations branches, latest is %s', count($results), $lastIntegrationBranch)
        );

        $statusResults = $this->runCommand('status -s');
        if (count($statusResults) > 0) {
            $symfonyStyle->error(array_merge(['Changes files must be committed first.'], $statusResults));

            return;
        }

        $this->runCommand('checkout ' . $lastIntegrationBranch);

        $currentBranch = $this->runCommand('rev-parse --abbrev-ref HEAD')[0];
        if ($currentBranch === $lastIntegrationBranch) {
            $this->runCommand('up'); // gp
            $symfonyStyle->success('Successfully checkout.');

            return;
        }

        $symfonyStyle->error(
            sprintf('Expected branch was %s, but you are on branch %s', $lastIntegrationBranch, $currentBranch)
        );
    }

    private function runCommand(string $command): array
    {
        $output = null;
        exec(sprintf('git %s', $command), $output);

        return $output;
    }
}