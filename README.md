# Console utils
## How checkout last integration branch

- Just run `composer install`
- Add alias to your `.profile`
````
function checkoutLastIntegration() {
	php ~/console-utils/bin/console git:checkout_last_integration
	g
}
````
- Run `. ~/.profile`
- Finally run `checkoutLastIntegration` in your directory project with git